'use strict'
import { loadAws, stopAWS } from './queue'
import { app, BrowserWindow, ipcMain } from 'electron'
const Config = require('electron-config')

//

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}
/*
  This is the settings page elements for the process
 */
let mainWindow
var filedata
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

const config = new Config()

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 700,
    useContentSize: true,
    width: 1250
  })

  mainWindow.loadURL(winURL)
  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

function getData () {
  filedata = config.get('data')
}

ipcMain.on('save', (event, data) => {
  /*
  This will take the data from the render view and save it to the file.
   */
  config.set('data', data)
})

ipcMain.on('getdata', (event, data) => {
  getData()
  event.sender.send('setup-data', filedata)
})

ipcMain.on('start_process', (event, data) => {
  /*
  This will be a message system which will start the queue system
   */
  if (data.status === 2) {
    stopAWS().then((data) => {
      event.sender.send('status', {'status': 0})
    })
  } else {
    getData()
    loadAws(filedata).then((data) => {
      console.log(data)
      event.sender.send('status', {'status': 2})
    }).catch(() => {
      event.sender.send('status', {'status': 1})
    })
  }
})
app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
