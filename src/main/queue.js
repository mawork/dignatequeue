const AWS = require('aws-sdk')
const https = require('https')
const http = require('http')
const fs = require('fs')
const hound = require('hound')
const Consumer = require('sqs-consumer')
const Producer = require('sqs-producer')

let params = {}
let systemdata = {}
let sqs
let queuelist
let queueName
let mainQueueUrl
let incomingQueue
let master
let client = http
let folderlist = []
let watchobjstruct = []
let watchobjelemt = {}
let s3
let lockfileloc = ''

function isInt (value) {
  return !isNaN(value) && (function (x) { return (x | 0) === x })(parseFloat(value))
}

function checklock () {
  console.log('check', lockfileloc)
  try {
    return (!!fs.lstatSync(lockfileloc))
  } catch (err) {
    return false
  }
}

function createlock () {
  console.log('createlock', lockfileloc)
  try {
    fs.writeFileSync(lockfileloc, '')
    let watch = hound.watch(lockfileloc)
    watch.on('delete', function (file, stat) {
      console.log(file, stat)
      incomingQueue.stop()
      incomingQueue.start()
    })
    return true
  } catch (err) {
    return false
  }
}

function removelock () {
  console.log('removelock', lockfileloc)
  try {
    return fs.unlinkSync(lockfileloc)
  } catch (err) {
    return false
  }
}

function getqueuesList () {
  /**
  This function goes through the queue url list and
  saves the url in the queuelist if it matches the queue_out
  Used only for Master client.
  */
  return new Promise((resolve, reject) => {
    sqs.listQueues(params, function (err, data) {
      if (err) {
        console.log(err)
        reject(err)
      } else {
        var newqueuelist = []
        if (data.QueueUrls) {
          data.QueueUrls.forEach((item, index) => {
            if (item.indexOf(systemdata.queue_out) > -1) {
              newqueuelist.push(Producer.create({
                queueUrl: item,
                region: systemdata.region,
                accessKeyId: systemdata.accessKeyId,
                secretAccessKey: systemdata.secretAccessKey
              }))
            }
          })
        }
        resolve(newqueuelist)
      }
    })
  })
}

function slugify (text) {
  /**
   * This will slugify the text
   */
  return text.toString().toLowerCase().trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '')
}

function generateQueueName () {
  /**
   * This gets the name of the queue which is going to be used.
   * If it is a master will use the one that is selected
   * If it is a slave will add the queue in with the computer name
   */
  if (systemdata.queue_in.endsWith('_')) {
    return systemdata.queue_in + slugify(systemdata.name) + '.fifo'
  } else {
    return systemdata.queue_in + '.fifo'
  }
}

function _listquuq () {
  /**
   * This takes the queue name then checks if there is a queue already there
   */
  return new Promise((resolve, reject) => {
    sqs.listQueues(params, function (err, data) {
      if (err) {
        reject(err)
      } else {
        let posibleququlist
        if (data.QueueUrls) {
          data.QueueUrls.forEach((item, index) => {
            if (item.endsWith(queueName)) {
              posibleququlist = item
            }
          })
        } else {
          reject(data)
        }
        if (posibleququlist !== undefined) {
          console.log('posibleququlist', posibleququlist)
          resolve(posibleququlist)
        } else {
          console.log('posibleququlist', 'reject', data)
          reject(data)
        }
      }
    })
  })
}

function _createQueue () {
  /**
   * This will create a new queue based on the queue name
   */
  return new Promise((resolve, reject) => {
    sqs.createQueue({QueueName: queueName, Attributes: { 'FifoQueue': 'true', 'ContentBasedDeduplication': 'true' }}, function (err, data) {
      if (err) {
        reject(err)
      } else {
        console.log('create', data.QueueUrl)
        resolve(data.QueueUrl)
      }
    })
  })
}

function getQueueIn () {
  /**
   * This takes the systendata,queue_in and gets the url for it or creates it
   */
  return new Promise((resolve, reject) => {
    queueName = generateQueueName()
    _listquuq().then((data) => {
      resolve(data)
    }).catch(() => {
      _createQueue().then((data) => {
        resolve(data)
      }, (err) => {
        reject(err)
      })
    })
  })
}

function queueLeastMessage () {
  /**
   * This will get the SQS queue with the least messages in currently
   */
  return new Promise((resolve, reject) => {
    let idealQueue = ''
    let messageCount
    let itemcount = 0
    queuelist.forEach((item, index) => {
      item.queueSize(function (err, size) {
        if (err) {
          console.log(err)
          reject(err)
        }
        if (!isInt(messageCount)) {
          messageCount = size
          idealQueue = item
        } else if (size < messageCount) {
          messageCount = size
          idealQueue = item
        }
        itemcount++
        if (itemcount === queuelist.length) {
          resolve(idealQueue)
        }
      })
    })
  })
}

function relayMessage (message) {
  /**
   * This is to take the message and get the queitest queue and send the message to it.
   */
  return new Promise((resolve, reject) => {
    queueLeastMessage().then((feed) => {
      feed.send([{ body: message.Body, id: 'id' + message.MessageId, groupId: 'mg' + message.MessageId }], function (err) {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve()
        }
      })
    }).catch((err) => {
      reject(err)
    })
  })
}

function download (url, dest, cb) {
  /**
   * This downloads the elements from a url to the required feed.
   * @type {"fs".WriteStream}
   */
  let file = fs.createWriteStream(dest)
  if (url.toString().indexOf('https://') === 0) {
    client = https
  } else {
    client = http
  }
  client.get(url, (resp) => {
    resp.pipe(file)
    file.on('finish', () => {
      file.close(cb)
    })
  }).on('error', (err) => {
    fs.unlink(dest)
    console.log(err)
    if (cb) {
      cb(err)
    }
  })
}

function getFileName (url) {
  /**
   * This splits a url into the file name
   */
  const splitUrl = url.split('/')
  const cleanSplit = splitUrl.filter((n) => { return n !== undefined && n !== '' })
  const lastelem = cleanSplit.pop()
  if (lastelem.indexOf('?') > -1) {
    return lastelem.split('?')[0]
  } else {
    return lastelem
  }
}

function downloadMessage (message) {
  /**
   * This will be the item which download the files to the system
   */
  const jsonBody = JSON.parse(message.Body)
  console.log('downloadMessage', systemdata.options)
  return new Promise((resolve, reject) => {
    for (const feed in systemdata.options) {
      let feedobj = systemdata.options[feed]
      for (const key of Object.keys(jsonBody)) {
        if (String(feedobj.feed).valueOf() === String(key).valueOf()) {
          if (!feedobj.monitor) {
            let filename = getFileName(jsonBody[key])
            download(jsonBody[key], feedobj.location + '/' + filename, console.log('aaa'))
          }
        }
      }
    }
    resolve()
  })
}

function setupQueue () {
  /**
   * This will pick up the messages from the watcher queue and
   * if it is master sending it over if slave download
   */
  incomingQueue = Consumer.create({
    queueUrl: mainQueueUrl,
    handleMessage: (message, done) => {
      console.log(message, 'locl', checklock())
      if (!checklock()) {
        createlock()
        if (master) {
          relayMessage(message).then(() => {
            done()
          })
        } else {
          console.log('slave', message)
          downloadMessage(message).then(() => {
            done()
          })
        }
      }
    },
    sqs: sqs
  })
  incomingQueue.on('error', (err) => {
    console.log(err.message)
  })
}

function getOptionFromLocation (location) {
  /**
   * This takes a folder directory and returns a systemdata option
   */
  return new Promise((resolve, reject) => {
    for (const feed in systemdata.options) {
      let feedobj = systemdata.options[feed]
      if (String(feedobj.location).valueOf() === String(location).valueOf()) {
        resolve(feedobj)
      }
    }
  })
}

function watchObjGet (name) {
  /**
   * This get the file name and works out the option name to create the
   * obj that will be sent back to the server
   */
  return new Promise((resolve, reject) => {
    let watchcounter
    for (const key of Object.keys(watchobjelemt)) {
      if (String(key).valueOf() === String(name).valueOf()) {
        watchcounter = name
        resolve(name)
      }
    }
    if (watchcounter === undefined) {
      watchobjelemt[name] = {}
      for (const item in watchobjstruct) {
        watchobjelemt[name][watchobjstruct[item]] = { 'file': '', 'object': {}, 's3': '' }
      }
      resolve(name)
    }
  })
}

function uploadToS3 (file, bucket, key) {
  return new Promise((resolve, reject) => {
    let uploadParams = { Bucket: bucket, Key: key + '/' + getFileName(file), Body: '' }
    let fileStream = fs.createReadStream(file)
    fileStream.on('error', (err) => {
      console.log(err)
      reject(err)
    })
    uploadParams.Body = fileStream
    s3.upload(uploadParams, (err, data) => {
      if (err) {
        console.log(err)
        reject(err)
      }
      if (data) {
        console.log(data)
        resolve(data)
      }
    })
  })
}

function uploadQueue (name) {
  /**
   * This isthe queue to send the messages
   */
  return new Promise((resolve, reject) => {
    console.log('uploadQueue', watchobjelemt[name])
    let elementcount = 0
    for (const watchelement of watchobjstruct) {
      console.log('uploadWatchElement_a', watchelement)
      console.log('uploadWatchElement_b', watchobjelemt[name][watchelement])
      let file = watchobjelemt[name][watchelement].file
      let bucket = watchobjelemt[name][watchelement].object.bucket
      let key = watchobjelemt[name][watchelement].object.key
      uploadToS3(file, bucket, key).then((data) => {
        watchobjelemt[name][watchelement].s3 = data.Location
        elementcount++
        if (elementcount === watchobjstruct.length) {
          resolve(name)
        }
      }).catch((err) => {
        console.log(err)
      })
    }
  })
}

function removeFiles (name) {
  return new Promise((resolve, reject) => {
    for (const watchelement of watchobjstruct) {
      fs.unlink(watchobjelemt[name][watchelement].file, (err) => {
        if (err) {
          console.log(err)
        } else {
          console.log('removed')
        }
      })
    }
    resolve()
  })
}

function uploadWatchElement (name) {
  /**
   * This get the watchobjelemt and uploads the element to S3 and sends message to server
   */
  console.log('uploadWatchElement', name)
  return new Promise((resolve, reject) => {
    console.log('uploadWatchElement', watchobjelemt[name])
    uploadQueue(name).then((data) => {
      console.log('uploadQueue', watchobjelemt[name])
      sendToSQS(name).then((data) => {
        console.log(watchobjelemt)
        removeFiles(name).then(() => {
          delete watchobjelemt[name]
          console.log(watchobjelemt)
          resolve(data)
        })
      }).catch((err) => {
        console.log(err)
        reject(err)
      })
    })
  })
}

function getSessionID (path) {
  console.log('getSessionID', path)
  if (path) {
    let filename = ''
    if (path.file !== undefined) {
      filename = path.file.split('/').pop()
    } else {
      filename = path.split('/').pop()
    }
    let name = filename.split('.')[0]
    return name
  }
  return 'session'
}

function sendToSQS (name) {
  /**
   * This sends the message to SQS for the server
   */
  return new Promise((resolve, reject) => {
    let sqaparam = {}
    for (const watchpara of watchobjstruct) {
      sqaparam[watchpara] = watchobjelemt[name][watchpara].s3
    }
    sqaparam['session_id'] = getSessionID(watchobjelemt[name][watchobjstruct[0]])
    queueLeastMessage().then((feed) => {
      feed.send([{ body: JSON.stringify(sqaparam), id: 'id' + watchobjelemt[name].id, groupId: 'mg' + watchobjelemt[name].id }], (err) => {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve()
        }
      })
    }).catch((err) => {
      reject(err)
    })
  })
}

function checkStructuretoSend () {
  /**
   * This goes through the watchobjelemt and sees which ones are ready to be sent.
   */
  for (const name of Object.keys(watchobjelemt)) {
    let locobj = watchobjelemt[name]
    let loccount = 0
    for (const locname of Object.keys(locobj)) {
      if (locobj[locname]['file'] !== '') {
        loccount++
        if (locobj[locname]['object']['error'] === true) {
          // this will send the object files to s3 and create the message to send to the server
          uploadWatchElement(name).then((resp) => {
            console.log(resp)
          }).catch((err) => {
            console.log('css-error', err)
          })
        }
      }
    }
    if (loccount === Object.keys(locobj).length) {
      // this will send the object files to s3 and create the message to send to the server
      uploadWatchElement(name).then((resp) => {
        console.log(resp)
      }).catch((err) => {
        console.log('css-error', err)
      })
    }
  }
}

function fileCreated (path) {
  /**
   * Works out the file saved and creates the folder object
   * @type {T[]}
   */
  let folderpath = path.split('/').slice(0, -1).join('/')
  let filename = path.split('/').pop()
  let name = filename.split('.')[0]
  watchObjGet(name).then((index) => {
    getOptionFromLocation(folderpath).then((feedobj) => {
      watchobjelemt[index][feedobj.feed].file = path
      watchobjelemt[index][feedobj.feed].object = feedobj
      console.log('fileCreated', watchobjelemt[index])
      checkStructuretoSend()
    })
  })
}

function setupFolderList () {
  /**
   * This gets the file list and creates the watch folders from the monitor elements on the folder list
   */
  return new Promise((resolve, reject) => {
    let counter = 0
    for (const feed in systemdata.options) {
      counter++
      let feedobj = systemdata.options[feed]
      if (feedobj.monitor) {
        if (feedobj.location !== '') {
          watchobjstruct.push(feedobj.feed)
          let watcher = hound.watch(feedobj.location)
          watcher.on('create', function (file, stat) {
            fileCreated(file)
            if (!stat) console.log('deleted')
          })
          folderlist.push(watcher)
        }
      }
    }
    if (counter === systemdata.options.length) {
      resolve(folderlist)
    }
  })
}

function setup () {
  sqs = new AWS.SQS(systemdata)
  s3 = new AWS.S3(systemdata)
}

function setupLock () {
  let lockloc = ''
  for (const feed in systemdata.options) {
    if (!systemdata.options[feed].monitor) {
      lockloc = systemdata.options[feed].location
    }
  }
  lockloc = lockloc + '/lock.lock'
  console.log('setupLock', lockloc)
  lockfileloc = lockloc
}

export function loadAws (data) {
  return new Promise((resolve, reject) => {
    AWS.config.update(data)
    systemdata = data
    setup()
    setupLock()
    getQueueIn().then((qdata) => {
      mainQueueUrl = qdata
      switch (systemdata.communication) {
        case 0:
          break
        case 1:
          // Master
          getqueuesList().then((qldata) => {
            queuelist = qldata
            console.log('queuelist', queuelist)
            master = true
            setupQueue()
            incomingQueue.start()
          }, (err) => {
            reject(err)
          })
          break
        case 2:
          // Slave
          master = false
          setupFolderList().then((fdata) => {
            console.log('setup', fdata)
          })
          getqueuesList().then((qldata) => {
            queuelist = qldata
            console.log('queuelist', queuelist)
            setupQueue()
            incomingQueue.start()
          })
          break
        default:
          break
      }
    }, (err) => {
      console.log(err)
      reject(err)
    })
    console.log('end element')
    resolve(true)
  })
}

export function stopAWS () {
  return new Promise((resolve, reject) => {
    incomingQueue.stop()
    sqs = undefined
    params = {}
    systemdata = {}
    queuelist = undefined
    queueName = undefined
    mainQueueUrl = undefined
    folderlist = []
    watchobjstruct = []
    watchobjelemt = {}
    console.log('stopAWS')
    resolve(true)
    removelock()
  })
}
